# Script variados de segurança


## Rodando os scripts

Você deve executar os scripts em ambiente Linux.

Para gerar uma Autoridade Certificadora, por exemplo, execute o script abaixo.

```bash
./create-ca.sh
```

## Listar as cadeias de Autoridades Certificadoras

Para listar as Autoridades Certificadoras de um arquivo, execute o script abaixo.

```bash
awk -v cmd='openssl x509 -noout -subject' '
    /BEGIN/{close(cmd)};{print | cmd}' < ca.crt
```

## Erros comuns

> Erro que podem acontecer se a clonagem do respositório for feita pelo Windows


```bash
./create-ca.sh
-bash: ./create-ca.sh: /bin/bash^M: bad interpreter: No such file or directory
```

The error message indicates that there is an issue with the interpreter specified in the shebang line (the first line) of the create-ca.sh script.

The ^M character at the end of the interpreter path (/bin/bash^M) suggests that the script was written in a Windows environment and has carriage return characters (\r) at the end of each line. Unix-based systems, such as Linux or macOS, use only line feed (\n) characters to denote the end of a line.

To fix this, you can convert the script to a Unix-compatible format by removing the carriage return characters. There are several ways to do this, but one easy method is to use the dos2unix command:

```bash
dos2unix create-ca.sh
```

This command will remove the carriage return characters and convert the file to a Unix-compatible format. Once you've done that, try running the script again.