#!/bin/bash

clear

JAVA_HOME=/home/07721825741/Documentos/Aplicativos/Java/jdk1.7.0_51

#NAO ALTERAR!
DSANAME="SERPRO"

#Apontar para o caminho em que o jar se encontra
JARPATH="/home/07721825741/NetBeansProjects/demoiselle-certificate-applet-customizada/target/"

#Nome do arquivo a ser assinado
JARFILE="demoiselle-certificate-applet-customizada-1.0.0.jar"

#Parametros customizados a serem incluidos no manifesto
manifest_addition="manifest-addition.txt"

#nome ddo arquivo depois de assinado
JARFILESIGNED="demoiselle-certificate-applet-customizada-1.0.0-assinado.jar"

#apelido do certificado, rodar o script uma primeira vez no console e copiar o apelido cara este script
ALIAS="(eTCAPI) HUMBERTO DE MELO PACHECO's ICP-Brasil ID"

#Efetua a leitura do Pin do Token/Smartcard
echo -n "Digite a senha do seu Token /Smartcard, seguido de [ENTER]:"

read -s PASSWORD

#lista os dados do token, inclusive o apelido
$JAVA_HOME/bin/keytool -keystore NONE -storetype PKCS11 -providerClass sun.security.pkcs11.SunPKCS11 -providerArg token.config -storepass $PASSWORD -list

#Inclui os dados adicionais de seguranca do jar
$JAVA_HOME/bin/jar uvfm $JARPATH/$JARFILE $manifest_addition

#assina o jar
$JAVA_HOME/bin/jarsigner -keystore NONE -storetype PKCS11 -providerClass sun.security.pkcs11.SunPKCS11 -providerArg token.config -storepass $PASSWORD -sigfile $DSANAME -signedjar $JARPATH/$JARFILESIGNED -verbose $JARPATH/$JARFILE "$ALIAS"
