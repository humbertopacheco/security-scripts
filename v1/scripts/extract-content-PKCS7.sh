#!/bin/bash

clear

PKCS7_FILE="texto.txt.p7s"

PKCS7_HOME=/home/07721825741

OPENSSL_HOME=/usr/local/ssl/

$OPENSSL_HOME/bin/openssl smime -pk7out -inform DER -in $PKCS7_HOME/$PKCS7_FILE > $PKCS7_HOME/extracted.txt

echo "Conteudo extraido em " $PKCS7_HOME/extracted.txt
