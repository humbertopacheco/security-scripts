# -exportcert  [-v] [-rfc] [-protected]
#	     [-alias <alias>] [-file <cert_file>]
#	     [-keystore <keystore>] [-storepass <storepass>]
#	     [-storetype <storetype>] [-providername <name>]
#	     [-providerclass <provider_class_name> [-providerarg <arg>]] ...
#	     [-providerpath <pathlist>]


#!/bin/bash

clear

JAVA_HOME=/home/07721825741/Documentos/Aplicativos/Java/jdk1.7.0_51

keytool -exportcert -v -rfc -alias tibco -file tibco.cer -keystore keystore-tibco.jks -storepass 123456
