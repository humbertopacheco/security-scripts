#!/bin/bash

clear


NAME_PRIVATE_KEY=privateKey.pem

NAME_CERTIFICATE=publicCert.pem

JAVA_HOME=/home/07721825741/Documentos/Aplicativos/Java/jdk1.6.0_37/bin

rm $NAME_PRIVATE_KEY

rm $NAME_CERTIFICATE

rm keystore-tibco.p12

#Conversao de JKS para P12
$JAVA_HOME/keytool -importkeystore -srckeystore keystore-tibco.jks -srcstoretype JKS -deststoretype PKCS12 -destkeystore keystore-tibco.p12 -srcstorepass 123456 -deststorepass 123456

# Extract private key from a p12 file and write it to PEM file

echo Extraindo a chave privada

openssl pkcs12 -in keystore-tibco.p12 -nocerts -out $NAME_PRIVATE_KEY

# Extract the certificate file (the signed public key) from the pfx file
openssl pkcs12 -in keystore-tibco.p12 -clcerts -nokeys -out $NAME_CERTIFICATE

rm keystore-tibco.p12
