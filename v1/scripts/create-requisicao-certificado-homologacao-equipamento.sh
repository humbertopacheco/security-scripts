#!/bin/bash

#  O DN dos certificados de equipamentos ou aplicação deverá conter:
#
#  C=BR 
#  O=ICP-Brasil 
#  OU=Autoridade Certificadora SERPROACF
#  OU=[Sigla do Órgão de trabalho] 
#  OU=Equipamento A1 
#  CN=[DNS do equipamento ou aplicação]

clear

NOME_KEYSTORE="keystore-homologacao.jks"

NOME_REQUISICAO="keystore-homologacao_req.pem"

export OPENSSL_CONF=/home/07721825741/Documentos/Scripts/openssl_servidor.cnf

ALIAS="tibco"

OPENSSL_HOME=/usr/local/ssl/misc/

JAVA_HOME=/home/07721825741/Documentos/Aplicativos/Java/jdk1.7.0_45

PASSWORD="changeit"

DNAME="CN=10.31.0.11, OU=Equipamento A1, OU=SERPRO, OU=Autoridade Certificadora SERPROACF, O=ICP-Brasil, C=BR"

rm $NOME_KEYSTORE

$JAVA_HOME/bin/keytool -genkeypair -alias $ALIAS -keyalg RSA -keysize 2048 -keystore $NOME_KEYSTORE -storepass $PASSWORD -dname "$DNAME"

$JAVA_HOME/bin/keytool -certreq -alias $ALIAS -keystore $NOME_KEYSTORE -storepass $PASSWORD -file $NOME_REQUISICAO

echo Imprimindo o Keystore...

$JAVA_HOME/bin/keytool -keystore $NOME_KEYSTORE -list -storepass $PASSWORD -v

echo Imprimindo a Requisicao de Certificado...

openssl req -noout -text -in $NOME_REQUISICAO

#$JAVA_HOME/bin/keytool -importcert -trustcacerts -alias $ALIAS -file newcert.pem -keystore $NOME_KEYSTORE -storepass $PASSWORD

