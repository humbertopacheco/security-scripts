#!/bin/bash

#Efetua a atualizacao do software Firefox. Nao esquecer de alterar para a versao
#mais nova antes de executar o script.

clear

firefox_folder="firefox-humberto"

#Efetua o  download do artefafo

#firefox_package="firefox-17.0.9esr.tar.bz2"
#wget http://download-installer.cdn.mozilla.net/pub/firefox/releases/17.0.9esr/linux-i686/pt-BR/$firefox_package

#firefox_package="firefox-24.7.0esr.tar.bz2"
#wget http://download-installer.cdn.mozilla.net/pub/firefox/releases/24.7.0esr/linux-i686/pt-BR/$firefox_package

#firefox_package="firefox-31.0esr.tar.bz2"
#wget http://download-installer.cdn.mozilla.net/pub/firefox/releases/31.0esr/linux-i686/pt-BR/$firefox_package

#firefox_package="firefox-32.0.tar.bz2"
#wget http://download-installer.cdn.mozilla.net/pub/firefox/releases/32.0/linux-i686/pt-BR/$firefox_package

#firefox_package="firefox-38.0.6.tar.bz2"
#wget http://download-installer.cdn.mozilla.net/pub/firefox/releases/38.0.6/linux-i686/pt-BR/$firefox_package

firefox_package="firefox-41.0.1.tar.bz2"
wget http://download-installer-origin.cdn.mozilla.net/pub/firefox/releases/41.0.1/linux-i686/pt-BR/$firefox_package

#Unpack the archive
tar xjf $firefox_package

#Remove the old
sudo rm -r /opt/firefox
sudo rm -r /opt/$firefox_folder

#Move to /opt
sudo mv firefox /opt/$firefox_folder

#Remove old sumbolic links
echo "Removendo o link simbolico antigo"
sudo rm /usr/bin/firefox

#Set up symbolic links
echo "Configurando o novo link simbolico"
sudo ln -s /opt/$firefox_folder/firefox /usr/bin/firefox

rm $firefox_package
