#!/bin/bash

clear

#Confira o OPENSSL.CFG para ver se as linhas corretas estao parametrizadas (185, 188, 217 e 219)  !

NOME_KEYSTORE="truststore.jks"

ALIASFAKE="foo"

COMMONNAME="CN=localhost"

ALIAS="STARTREKCA"

OPENSSL_HOME=/usr/lib/ssl/misc/

JAVA_HOME=/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_112/bin

CASERPROPRD_HOME="CAPRODUCAO"

CASERPROHOM_HOME="CAHOMOLOGACAO"

rm $NOME_KEYSTORE

$JAVA_HOME/keytool -genkeypair -alias $ALIASFAKE -keyalg RSA -keystore $NOME_KEYSTORE -storepass changeit -dname $COMMONNAME -v

$JAVA_HOME/keytool -delete -alias $ALIASFAKE -keystore $NOME_KEYSTORE -storepass changeit

cp demoCA/cacert.pem $CASERPROPRD_HOME

echo "Removendo antigas cadeias de producao"

$JAVA_HOME/keytool -delete -alias $ALIAS -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias acserprov3 -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias ICP-Brasil -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias icpbrasilv2 -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias raiz.brasil -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serproacfv3 -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serpro.final -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serprofinalv2 -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serprov1 -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serprov2 -keystore $NOME_KEYSTORE -storepass changeit

echo "Removendo antigas cadeias de homologacao"

$JAVA_HOME/keytool -delete -alias acserprov3HOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias ICP-BrasilHOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias icpbrasilv2HOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias raiz.brasilHOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serproacfv3HOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serpro.finalHOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serprofinalv2HOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serprov1HOM -keystore $NOME_KEYSTORE -storepass changeit
$JAVA_HOME/keytool -delete -alias serprov2HOM -keystore $NOME_KEYSTORE -storepass changeit

echo "Adicionando novas cadeias de producao"

$JAVA_HOME/keytool -v -importcert -trustcacerts -alias $ALIAS -file $CASERPROPRD_HOME/cacert.pem -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias icpbrasilv2 -file $CASERPROPRD_HOME/icpbrasilv2.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias acserprov3 -file $CASERPROPRD_HOME/acserprov3.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serproacfv3 -file $CASERPROPRD_HOME/serproacfv3.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias ICP-Brasil -file $CASERPROPRD_HOME/ICP-Brasil.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias raiz.brasil -file $CASERPROPRD_HOME/raiz.brasil.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serpro.final -file $CASERPROPRD_HOME/serpro.final.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serprofinalv2 -file $CASERPROPRD_HOME/serprofinalv2.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serprov1 -file $CASERPROPRD_HOME/serprov1.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serprov2 -file $CASERPROPRD_HOME/serprov2.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt

echo "Adicionando novas cadeias de homologacao"

$JAVA_HOME/keytool -v -importcert -trustcacerts -alias acserprov3HOM -file $CASERPROHOM_HOME/acserprov3.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias ICP-BrasilHOM -file $CASERPROHOM_HOME/ICP-Brasil.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias icpbrasilv2HOM -file $CASERPROHOM_HOME/icpbrasilv2.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias raiz.brasilHOM -file $CASERPROHOM_HOME/raiz.brasil.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serproacfv3HOM -file $CASERPROHOM_HOME/serproacfv3.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serpro.finalHOM -file $CASERPROHOM_HOME/serpro.final.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serprofinalv2HOM -file $CASERPROHOM_HOME/serprofinalv2.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serprov1HOM -file $CASERPROHOM_HOME/serprov1.cer -keystore $NOME_KEYSTORE -storepass changeit -noprompt
$JAVA_HOME/keytool -v -importcert -trustcacerts -alias serprov2HOM -file $CASERPROHOM_HOME/serprov2.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt

#keytool -keystore $NOME_KEYSTORE -list -v -storepass changeit

rm $CASERPROPRD_HOME/cacert.pem
