#!/bin/bash

clear

#Confira o OPENSSL.CFG para ver se as linhas corretas estao parametrizadas (185, 188, 217 e 219)  !

NOME_KEYSTORE="keystore.jks"

ALIAS="jimkirk"

#OPENSSL_HOME=/usr/local/ssl/misc/
OPENSSL_HOME=/usr/lib/ssl/misc/

export JAVA_HOME=/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_45

export OPENSSL_CONF=/home/07721825741/Documentos/Trabalho/Scripts/openssl_server.cnf

PASSWORD="changeit"

DNAME="CN=localhost,OU=SUPST,O=SERPRO,C=BR"
#DNAME="CN=10.34.177.125"
#DNAME="CN=10.34.176.250"
#DNAME="CN=localhost"

VALIDITY=365

rm $NOME_KEYSTORE

$JAVA_HOME/bin/keytool -genkeypair -alias $ALIAS -keyalg RSA -keystore $NOME_KEYSTORE -storepass $PASSWORD -dname $DNAME -validity $VALIDITY -v

$JAVA_HOME/bin/keytool -certreq -alias $ALIAS -keystore $NOME_KEYSTORE -storepass $PASSWORD -file newreq.pem

perl $OPENSSL_HOME/CA.pl -sign

openssl verify -verbose -CAfile demoCA/cacert.pem newcert.pem

echo "O arquivo \"newcert.pem\" sera importado. Continuar? [ENTER]"

read EDITOK

$JAVA_HOME/bin/keytool -importcert -trustcacerts -alias $ALIAS -file newcert.pem -keystore $NOME_KEYSTORE -storepass $PASSWORD

echo "----------------------------------------------------------------------"

$JAVA_HOME/bin/keytool -keystore $NOME_KEYSTORE -list -storepass $PASSWORD -v

rm newreq.pem

rm newcert.pem
