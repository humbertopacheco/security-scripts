#!/bin/bash

clear

OPENSSL_HOME=/usr/local/ssl/misc/

NOME_CRL=enterprise.crl

openssl ca -verbose -gencrl -out $NOME_CRL -crldays 1

cp $NOME_CRL demoCA/crl/ 

rm $NOME_CRL
