#!/bin/bash

clear

OPENSSL_HOME=/usr/local/ssl/misc/

openssl ca -revoke newcert.pem -keyfile demoCA/private/cakey.pem -cert demoCA/cacert.pem

openssl ca -gencrl -keyfile demoCA/private/cakey.pem -cert demoCA/cacert.pem -out enterprisecrl.pem

openssl crl -in enterprisecrl.pem -outform DER -out enterprisecrl.der

openssl crl -in enterprisecrl.pem  -noout -text
