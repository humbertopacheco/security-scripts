#!/bin/bash

#Efetua a validacao de um arquivo assinado no formato PKCS#7

clear

pkcs7_package="openssl-1.0.0l.tar.gz"

pkcs7_folder="/home/07721825741"

openssl pkcs7 -inform DER -in $pkcs7_folder/$pkcs7_package -text -out resumo.txt

openssl pkcs7 -inform DER -in $pkcs7_folder/$pkcs7_package -text -print_certs -out resumo_com_certificados.txt
