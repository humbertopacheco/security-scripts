#!/bin/bash

clear

ALIAS_CA=startrek_CA

#OPENSSL_HOME=/usr/local/ssl
OPENSSL_HOME=/usr/lib/ssl

SCRIPTS_HOME=/home/07721825741/NetBeansProjects/security-scripts/scripts

JAVA_HOME=/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_121

KEYSTORE_HOME=$JAVA_HOME/jre/lib/security/cacerts

export OPENSSL_CONF=$SCRIPTS_HOME/openssl_server.cnf

$JAVA_HOME/bin/keytool -delete -alias $ALIAS_CA -keystore $KEYSTORE_HOME -storepass changeit

rm demoCA/ -R

echo "AVISO: A senha padrao da chave privada eh \"secret\""

echo "Autoridade Certificadora antiga foi removida. Criar nova?[ENTER]"

read OK

perl $OPENSSL_HOME/misc/CA.pl -newca

echo "Copiando o arquivo cacert.pem"

cp demoCA/cacert.pem .

openssl x509 -purpose -in cacert.pem

echo "Esta CA sera registrada como confiavel pelo JAVA.Continuar ? [ENTER]"
read EDITOK

$JAVA_HOME/bin/keytool -importcert -trustcacerts -alias $ALIAS_CA -file cacert.pem -keystore $KEYSTORE_HOME -storepass changeit -noprompt

#echo "A seguir serao listadas todas as CAs confiaveis pelo JAVA. [ENTER]"
#read EDITOK

#keytool -list -keystore $KEYSTORE_HOME -storepass changeit

rm cacert.pem
