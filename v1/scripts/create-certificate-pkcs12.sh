#!/bin/bash

clear

# Check if newcert.p12 exists and remove it if it does
if [ -e newcert.p12 ]
then
    rm newcert.p12
    echo "Removed existing newcert.p12 file"
fi


OPENSSL_HOME=/usr/lib/ssl
OPENSSL_HOME2=/usr/bin

DEFAULT_ALIAS="spock"

read -p "Current value of DEFAULT_ALIAS is $DEFAULT_ALIAS. Do you want to change it? (y/n) " change_default_alias

# Change variable values if requested
if [[ $change_default_alias == "y" ]]; then
    read -p "Enter new value for DEFAULT_ALIAS: " DEFAULT_ALIAS
fi

# Display updated variable values
echo "DEFAULT_ALIAS is now set to: $DEFAULT_ALIAS"


SCRIPTS_HOME=/home/07721825741/NetBeansProjects/security-scripts/scripts

file=$( dialog --clear --stdout --menu 'Escolha a opção:' 0 0 0 \
1 "Emitir certificado de pessoa fisica" \
2 "Emitir certificado de pessoa juridica" \
3 "Emitir certificado de equipamento" )

case $file in
   1)
      export OPENSSL_CONF=$SCRIPTS_HOME/openssl_e_cpf.cnf;;
   2)
      export OPENSSL_CONF=$SCRIPTS_HOME/openssl_e_cnpj.cnf;;
   3)
      export OPENSSL_CONF=$SCRIPTS_HOME/openssl_equipamento;;
   *)
      clear
      exit;;
esac

perl $OPENSSL_HOME/misc/CA.pl -newreq

perl $OPENSSL_HOME/misc/CA.pl -sign

 $OPENSSL_HOME2/openssl verify -verbose -CAfile demoCA/cacert.pem newcert.pem

perl $OPENSSL_HOME/misc/CA.pl -pkcs12 $DEFAULT_ALIAS

 $OPENSSL_HOME2/openssl pkcs12 -in newcert.p12 -info

rm newreq.pem

rm newcert.pem

rm newkey.pem
