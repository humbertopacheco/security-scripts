#!/bin/bash

clear

JAVA_HOME=/home/07721825741/Documentos/Aplicativos/Java/jdk1.7.0_25

#Conversao de JKS para P12
#keytool -importkeystore -srckeystore keystore.jks -srcstoretype JKS -deststoretype PKCS12 -destkeystore keystore-converted.p12

#Conversao de P12 para JKS
keytool -importkeystore -srckeystore painelexecutivo.serpro.gov.br.p12 -srcstoretype PKCS12 -deststoretype JKS -destkeystore painelexecutivo.serpro.gov.br.jks -srcalias 1 -destalias painel -srckeypass changeit -destkeypass changeit
