#!/bin/bash

clear

JAVA_HOME=/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_121

NOME_KEYSTORE=$JAVA_HOME/jre/lib/security/cacerts

STOREPASS="changeit"

#Baixa arquivo completo do site do ITI
wget http://acraiz.icpbrasil.gov.br/credenciadas/CertificadosAC-ICP-Brasil/ACcompactado.zip

#Baixa o hash do arquivo para validacao
wget http://acraiz.icpbrasil.gov.br/credenciadas/CertificadosAC-ICP-Brasil/hashsha512.txt

sha512sum -c hashsha512.txt

mkdir cadeias

unzip -d cadeias ACcompactado.zip

CA_FILES=cadeias/*
for f in $CA_FILES
do
  echo "Processing $f Certificate..."
  # take action on each file. $f store current file name

  $JAVA_HOME/bin/keytool -delete -alias $f -keystore $NOME_KEYSTORE -storepass $STOREPASS -noprompt

  $JAVA_HOME/bin/keytool -importcert -trustcacerts -alias $f -file $f -keystore $NOME_KEYSTORE -storepass $STOREPASS -noprompt
done

rm cadeias/ -R

rm ACcompactado.zip

rm hashsha512.txt

keytool -keystore $NOME_KEYSTORE -list -storepass $STOREPASS
