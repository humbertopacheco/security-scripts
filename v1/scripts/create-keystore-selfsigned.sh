#!/bin/bash

clear

NOME_KEYSTORE="keystore.jks"

ALIAS="foo"

JAVA_HOME=/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_101

DNAME="CN=localhost,OU=SUPDE,O=SERPRO,C=BR"
#DNAME="CN=10.34.177.191"
#DNAME="CN=10.34.177.139"

VALDAYS=365

STOREPASS="changeit"

rm $NOME_KEYSTORE

$JAVA_HOME/bin/keytool -genkeypair -alias $ALIAS -keyalg RSA -keystore $NOME_KEYSTORE -storepass $STOREPASS -dname $DNAME -validity $VALDAYS -v

$JAVA_HOME/bin/keytool -keystore $NOME_KEYSTORE -list -v -storepass $STOREPASS
