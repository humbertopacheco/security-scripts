#!/bin/bash

clear

JAVA_HOME=/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_121

NOME_KEYSTORE=$JAVA_HOME/jre/lib/security/cacerts

ALIAS="STARTREKCA"

CASERPRO_HOME="CAPRODUCAO"

cp demoCA/cacert.pem $CASERPRO_HOME

#Cadeia da Autoridade Certificadora Fake
keytool -delete -alias $ALIAS -keystore $NOME_KEYSTORE -storepass changeit


#Cadeias do SERPRO
keytool -delete -alias acserproacfv4 -keystore $NOME_KEYSTORE -storepass changeit
keytool -delete -alias acserprov3 -keystore $NOME_KEYSTORE -storepass changeit
keytool -delete -alias ICP-Brasil -keystore $NOME_KEYSTORE -storepass changeit
keytool -delete -alias icpbrasilv2 -keystore $NOME_KEYSTORE -storepass changeit
keytool -delete -alias serproacfv3 -keystore $NOME_KEYSTORE -storepass changeit
keytool -delete -alias serprofinalv2 -keystore $NOME_KEYSTORE -storepass changeit
keytool -delete -alias serprov2 -keystore $NOME_KEYSTORE -storepass changeit

echo "Edite o arquivo cacert.pem para a importacao. Continuar?"

read EDITOK

echo "Importar Cadeias?"

read EDITOK

keytool -v -importcert -trustcacerts -alias $ALIAS -file $CASERPRO_HOME/cacert.pem -keystore $NOME_KEYSTORE -storepass changeit -noprompt

keytool -v -importcert -trustcacerts -alias acserproacfv4 -file $CASERPRO_HOME/acserproacfv4.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
keytool -v -importcert -trustcacerts -alias acserprov3 -file $CASERPRO_HOME/acserprov3.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
keytool -v -importcert -trustcacerts -alias ICP-Brasil -file $CASERPRO_HOME/ICP-Brasil.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
keytool -v -importcert -trustcacerts -alias icpbrasilv2 -file $CASERPRO_HOME/icpbrasilv2.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
keytool -v -importcert -trustcacerts -alias serproacfv3 -file $CASERPRO_HOME/serproacfv3.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
keytool -v -importcert -trustcacerts -alias serprofinalv2 -file $CASERPRO_HOME/serprofinalv2.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt
keytool -v -importcert -trustcacerts -alias serprov2 -file $CASERPRO_HOME/serprov2.crt -keystore $NOME_KEYSTORE -storepass changeit -noprompt

