#!/bin/bash


#=======================================================================
# Criação de um arquivo PKCS12 contendo varias entradas de certificados
#=======================================================================

clear

#Confira o OPENSSL.CFG para ver se as linhas corretas estao parametrizadas (185, 188, 217 e 219)  !

OPENSSL_HOME=/usr/lib/ssl/misc/

JAVA_HOME=/home/07721825741/Documentos/Aplicativos/Java/jdk1.6.0_35

NOME_KEYSTORE="keystore.jks"

NOME_PKCS12="multientry.p12"

STOREPASS="changeit"

VALDAYS="365"

ALIASES=("James-Tiberius-Kirk" "Spock" "Leonard-McCoy" "Pavel-Chekov")

rm $NOME_KEYSTORE
rm $NOME_PKCS12

for ALIAS in "${ALIASES[@]}";do

        echo $ALIAS

        keytool -genkeypair -alias $ALIAS -keyalg RSA -keystore $NOME_KEYSTORE -keypass $STOREPASS -storepass $STOREPASS -dname "CN=\"${ALIAS}\"" -validity $VALDAYS -v
        
        keytool -certreq -alias $ALIAS -keystore $NOME_KEYSTORE -storepass $STOREPASS -file newreq.pem

        perl $OPENSSL_HOME/CA.pl -sign

        openssl verify -CAfile demoCA/cacert.pem newcert.pem

        #Criar edicao do arquivo -----BEGIN CERTIFICATE-----

        echo "Edite o arquivo newcert.pem para a importacao. [ENTER] para continuar..."

        read EDITOK

        keytool -importcert -trustcacerts -alias $ALIAS -file newcert.pem -keystore $NOME_KEYSTORE -storepass $STOREPASS 

        keytool -keystore $NOME_KEYSTORE -storepass $STOREPASS -list -v

        #keytool -importkeystore -srckeystore $NOME_KEYSTORE -destkeystore $NOME_PKCS12 -srcstoretype JKS -deststoretype PKCS12 -srcstorepass $STOREPASS -deststorepass $STOREPASS -srcalias $ALIAS -destalias $ALIAS -srckeypass $STOREPASS -destkeypass $STOREPASS -noprompt

        rm newreq.pem

        rm newcert.pem

done

exit
