#!/bin/bash

clear

JAVA_HOME=/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_121

CHROME_HOME=/opt/google/chrome/plugins

FIREFOX_HOME=/usr/lib/mozilla/plugins

echo "Efetuando a atualizacao do Google Chrome"

echo "Removendo o link antigo..."

sudo rm $CHROME_HOME/libnpjp2.so

echo "Recriando o link..."
sudo ln -s $JAVA_HOME/jre/lib/i386/libnpjp2.so $CHROME_HOME

echo "Efetuando a atualizacao do Mozilla Firefox"

sudo rm $FIREFOX_HOME/libnpjp2.so

echo "Recriando o link..."
sudo ln -s $JAVA_HOME/jre/lib/i386/libnpjp2.so $FIREFOX_HOME

echo "Finalizado."
