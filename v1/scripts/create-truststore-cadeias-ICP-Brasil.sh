#!/bin/bash

clear

NOME_KEYSTORE="icpbrasil.jks"

ALIASFAKE="foo"

COMMONNAME="CN=localhost"

JAVA_HOME="/home/07721825741/Documentos/Trabalho/Aplicativos/Java/jdk1.8.0_101/bin"

STOREPASS="changeit"
#STOREPASS="security-certificate"

rm $NOME_KEYSTORE

#Baixa arquivo completo do site do ITI
wget http://acraiz.icpbrasil.gov.br/credenciadas/CertificadosAC-ICP-Brasil/ACcompactado.zip

#Baixa o hash do arquivo para validacao
wget http://acraiz.icpbrasil.gov.br/credenciadas/CertificadosAC-ICP-Brasil/hashsha512.txt

sha512sum -c hashsha512.txt

mkdir cadeias

unzip -d cadeias ACcompactado.zip

$JAVA_HOME/keytool -genkeypair -alias $ALIASFAKE -keyalg RSA -keystore $NOME_KEYSTORE -storepass $STOREPASS -dname $COMMONNAME -v

$JAVA_HOME/keytool -delete -alias $ALIASFAKE -keystore $NOME_KEYSTORE -storepass $STOREPASS

CA_FILES=cadeias/*
for f in $CA_FILES
do
  echo "Processing $f Certificate..."
  # take action on each file. $f store current file name
  $JAVA_HOME/keytool -importcert -trustcacerts -alias $f -file $f -keystore $NOME_KEYSTORE -storepass $STOREPASS -noprompt
done

rm cadeias/ -R

rm ACcompactado.zip

rm hashsha512.txt

keytool -keystore $NOME_KEYSTORE -list -storepass $STOREPASS
