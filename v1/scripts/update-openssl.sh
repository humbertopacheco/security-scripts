#!/bin/bash

#Efetua a atualizacao do software openssl. Nao esquecer de alterar para a versao
#mais nova antes de executar o script.

clear

openssl_package="openssl-1.0.1i.tar.gz"

openssl_folder="openssl-1.0.1i"

openssl_hash="openssl-1.0.1i.tar.gz.sha1"

#Mostra a versao do openssl instalada no momento
openssl version

wget https://www.openssl.org/source/$openssl_package

wget https://www.openssl.org/source/$openssl_hash


sha1sum openssl-1.0.1g.tar.gz

cat $openssl_hash

#Unpack the archive and install
tar xzvf $openssl_package

cd $openssl_folder

./config

make

make test

sudo make install

rm ../$openssl_folder -R

rm ../$openssl_package

rm ../$openssl_hash

#Mostra a versao do openssl atualizada
/usr/local/ssl/bin/openssl version
