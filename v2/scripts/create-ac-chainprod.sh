#!/bin/bash

# Set the name of the downloaded file and the folder to extract to
FILE_NAME="ACcompactado.zip"
FILE_NAME_HASH="hashsha512.txt"
EXTRACTED_FOLDER="ACcompactado"


# Set the URL of the file to download and the hash file to check against
URL="https://acraiz.icpbrasil.gov.br/credenciadas/CertificadosAC-ICP-Brasil/$FILE_NAME"
HASH_URL="https://acraiz.icpbrasil.gov.br/credenciadas/CertificadosAC-ICP-Brasil/$FILE_NAME_HASH"


# Download the zip file from the URL
wget $URL -O $FILE_NAME --no-check-certificate

# Download the hash file
wget $HASH_URL -O $FILE_NAME_HASH --no-check-certificate

# Extract the expected SHA512 hash from the hash file
EXPECTED_HASH=$(cat $FILE_NAME_HASH | cut -d ' ' -f1)


# Calculate the SHA512 hash of the downloaded zip file
CALCULATED_HASH=$(openssl dgst -sha512 $FILE_NAME | cut -d' ' -f2)

# Print the expected and calculated hash values
echo "Expected hash: $EXPECTED_HASH"
echo "Calculated hash: $CALCULATED_HASH"

# Compare the expected and calculated hash values
if [ "$EXPECTED_HASH" == "$CALCULATED_HASH" ]
then
  echo "Hash check passed. Downloaded file is valid."

  # Unzip the file to the specified folder
  unzip $FILE_NAME -d $EXTRACTED_FOLDER


  touch ca.crt

  for file in $EXTRACTED_FOLDER/*.crt; do
    if [ -f "$file" ]; then
      echo "Importing $file as a trusted certificate..."
    cat $file >> ca.crt
      echo "Done."
    fi
  done

  # Remove the folder and the zip file
  rm -rf $EXTRACTED_FOLDER $FILE_NAME $FILE_NAME_HASH

else
  echo "Hash check failed. Downloaded file may be corrupted or tampered with."
fi
