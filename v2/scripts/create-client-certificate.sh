#!/bin/bash

# Variables
CA_DIR="./demoCA"
CLIENT_DIR="./client"
CLIENT_KEY="${CLIENT_DIR}/client.key"
CLIENT_CSR="${CLIENT_DIR}/client.csr"
CLIENT_CERT="${CLIENT_DIR}/client.crt"
CLIENT_PKCS12="${CLIENT_DIR}/client.p12"
CLIENT_PASS=changeit
CA_CERT="${CA_DIR}/ca.crt"
CA_KEY="${CA_DIR}/ca.key"
OPENSSL_CONFIG="../config/openssl.cnf"
EXTENSIONS_CONFIG="../config/equipamento-extensions.txt"

DEFAULT_COUNTRY="BR"
DEFAULT_STATE="RJ"
DEFAULT_CITY="Rio de Janeiro"
DEFAULT_ORG="DETRAN RJ"
DEFAULT_ORG_UNIT="DETRAN"
DEFAULT_DOMAIN="OIC DETRAN RJ"

# Check if client directory exists
if [ -d "${CLIENT_DIR}" ]; then
  # Remove all content from the directory
  rm -r "${CLIENT_DIR}"/*
else
  # Create the directory if it doesn't exist
  mkdir -p "${CLIENT_DIR}"
fi


# Generate client private key
openssl genrsa -out "${CLIENT_KEY}" 2048

read -p "Enter the two-letter country code for the client (default: ${DEFAULT_COUNTRY}): " COUNTRY
COUNTRY=${COUNTRY:-${DEFAULT_COUNTRY}}

read -p "Enter the state or province name for the client (default: ${DEFAULT_STATE}): " STATE
STATE=${STATE:-${DEFAULT_STATE}}

read -p "Enter the city or locality name for the client (default: ${DEFAULT_CITY}): " CITY
CITY=${CITY:-${DEFAULT_CITY}}

read -p "Enter the name of the client's organization (default: ${DEFAULT_ORG}): " ORG
ORG=${ORG:-${DEFAULT_ORG}}

read -p "Enter the name of the client's organizational unit (default: ${DEFAULT_ORG_UNIT}): " ORG_UNIT
ORG_UNIT=${ORG_UNIT:-${DEFAULT_ORG_UNIT}}

read -p "Enter the fully qualified domain name for the client (default: ${DEFAULT_DOMAIN}): " DOMAIN
DOMAIN=${DOMAIN:-${DEFAULT_DOMAIN}}

# Generate client CSR
openssl req -new -key "${CLIENT_KEY}" -out "${CLIENT_CSR}" -config "${OPENSSL_CONFIG}" -subj "/C=${COUNTRY}/ST=${STATE}/L=${CITY}/O=${ORG}/OU=${ORG_UNIT}/CN=${DOMAIN}"

# Sign client CSR with CA
openssl x509 -req -in "${CLIENT_CSR}" -CA "${CA_CERT}" -CAkey "${CA_KEY}" -CAcreateserial -out "${CLIENT_CERT}" -days 365 -sha256 -extensions extra_ext -extfile "${EXTENSIONS_CONFIG}"

# Export client certificate and key to PKCS#12 format
openssl pkcs12 -export -in "${CLIENT_CERT}" -inkey "${CLIENT_KEY}" -out "${CLIENT_PKCS12}" -passout pass:$CLIENT_PASS

# Display PKCS#12 information
openssl pkcs12 -info -in "${CLIENT_PKCS12}" -passin pass:$CLIENT_PASS

# Display CRT information
openssl x509 -in "${CLIENT_CERT}" -text -noout

# Remove the CSR file
rm "${CLIENT_CSR}"