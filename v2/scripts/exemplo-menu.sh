#!/bin/bash

# Set default values
myvar1="default1"
myvar2="default2"

# Ask user if they want to change the values
read -p "Current value of myvar1 is $myvar1. Do you want to change it? (y/n) " change_var1
read -p "Current value of myvar2 is $myvar2. Do you want to change it? (y/n) " change_var2

# Change variable values if requested
if [[ $change_var1 == "y" ]]; then
    read -p "Enter new value for myvar1: " myvar1
fi

if [[ $change_var2 == "y" ]]; then
    read -p "Enter new value for myvar2: " myvar2
fi

# Display updated variable values
echo "myvar1 is now set to: $myvar1"
echo "myvar2 is now set to: $myvar2"

# Display menu options
option=""
while [[ $option != "q" ]]
do
    echo "Select an option:"
    echo "1. Option 1"
    echo "2. Option 2"
    echo "3. Option 3"
    echo "q. Quit"

    # Read user input
    read -p "Enter option: " option

    # Handle user input
    case "$option" in
        "1")
            echo "Option 1 selected"
            ;;
        "2")
            echo "Option 2 selected"
            ;;
        "3")
            echo "Option 3 selected"
            ;;
        "q")
            echo "Quitting..."
            ;;
        *)
            echo "Invalid option"
            ;;
    esac
done
