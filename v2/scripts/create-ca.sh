#!/bin/bash

# Set the CA directory
CA_DIR="./demoCA"

# Set the OpenSSL configuration file path
# OPENSSL_CONFIG="/etc/ssl/openssl.cnf"
OPENSSL_CONFIG="../config/openssl.cnf"

# Set the default values for the certificate information
COUNTRY_DEFAULT="CA"
STATE_DEFAULT="Ontario"
CITY_DEFAULT="Toronto"
ORG_DEFAULT="Acme Inc."
OU_DEFAULT="Technology"
COMMON_NAME_DEFAULT="Acme Certificate Authority"
EMAIL_DEFAULT="admin@acme.com"

# Remove and recreate CA directory
rm -rf "$CA_DIR"
mkdir -p "$CA_DIR/newcerts"

mkdir -p "$CA_DIR"

# Create the serial file
echo 1000 > "$CA_DIR/serial"

# Create the crlnumber file
echo 1000 > "$CA_DIR/crlnumber"

# Create the index.txt file
touch "$CA_DIR/index.txt"

# Prompt the user for the certificate information
read -rp "Country Name (2 letter code) [$COUNTRY_DEFAULT]: " COUNTRY
COUNTRY=${COUNTRY:-$COUNTRY_DEFAULT}
read -rp "State or Province Name (full name) [$STATE_DEFAULT]: " STATE
STATE=${STATE:-$STATE_DEFAULT}
read -rp "Locality Name (eg, city) [$CITY_DEFAULT]: " CITY
CITY=${CITY:-$CITY_DEFAULT}
read -rp "Organization Name (eg, company) [$ORG_DEFAULT]: " ORG
ORG=${ORG:-$ORG_DEFAULT}
read -rp "Organizational Unit Name (eg, IT department) [$OU_DEFAULT]: " OU
OU=${OU:-$OU_DEFAULT}
read -rp "Common Name (eg, your name or your server's hostname) [$COMMON_NAME_DEFAULT]: " COMMON_NAME
COMMON_NAME=${COMMON_NAME:-$COMMON_NAME_DEFAULT}
read -rp "Email Address [$EMAIL_DEFAULT]: " EMAIL
EMAIL=${EMAIL:-$EMAIL_DEFAULT}

# Generate a private key for the CA
openssl genrsa -out "$CA_DIR/ca.key" 4096

# Create a new root certificate for the CA
openssl req -new -x509 -days 3650 \
  -key "$CA_DIR/ca.key" \
  -out "$CA_DIR/ca.crt" \
  -subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$ORG/OU=$OU/CN=$COMMON_NAME/emailAddress=$EMAIL" \
  -config "$OPENSSL_CONFIG"

# Display the CA certificate information
openssl x509 -in "$CA_DIR/ca.crt" -noout -text

# Generate a CRL for the CA
openssl ca -gencrl \
  -keyfile "$CA_DIR/ca.key" \
  -cert "$CA_DIR/ca.crt" \
  -out "$CA_DIR/ca.crl" \
  -config "$OPENSSL_CONFIG"

# Display the CRL information
openssl crl -in "$CA_DIR/ca.crl" -noout -text