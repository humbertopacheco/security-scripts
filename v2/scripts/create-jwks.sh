#!/bin/bash

# Check if ca_for_jwk folder exists
if [ ! -d "ca_for_jwk" ]; then
  echo "ca_for_jwk folder not found."
  exit 1
fi

# Create jwk directory if it doesn't exist
if [ ! -d "jwk" ]; then
  mkdir jwk
fi

# Initialize empty array for JWKs
jwks=()

# Iterate over certificate files in ca_for_jwk directory
for crt_file in ca_for_jwk/*.crt
do
  # Extract public key from certificate
  openssl x509 -in "$crt_file" -noout -pubkey > public_key.pem

  # Convert public key to JWK format
  modulus=$(openssl rsa -in public_key.pem -pubin -modulus -noout | cut -d'=' -f2 | tr -d '[:space:]' | xxd -r -p | base64 | tr -d '\n')
  # exponent=$(openssl rsa -in public_key.pem -pubin -text -noout | grep "Exponent" | cut -d':' -f2 | tr -d '[:space:]' | xxd -r -p | base64 | tr -d '\n')
  exponent=$(openssl rsa -in public_key.pem -pubin -text -noout | grep "Exponent" | awk '{print $2}')

  kid=$(openssl x509 -in "$crt_file" -noout -serial | cut -d'=' -f2 | tr '[:upper:]' '[:lower:]')
  alg="RS256"
  use="sig"
  thumbprint=$(openssl x509 -in "$crt_file" -noout -fingerprint -sha1 | cut -d'=' -f2 | tr -d ':' | tr '[:upper:]' '[:lower:]')
  cert=$(openssl x509 -in "$crt_file" -outform DER | base64 | tr -d '\n')

  # Construct JWK and add to array
  jwk=$(jq -n --arg kty "RSA" \
            --arg n "$modulus" \
            --arg e "$exponent" \
            --arg alg "$alg" \
            --arg use "$use" \
            --arg kid "$kid" \
            --arg x5t "$thumbprint" \
            --arg x5c "$cert" \
            '{"kty":$kty,"n":$n,"e":$e,"alg":$alg,"use":$use,"kid":$kid,"x5t":$x5t,"x5c":[$x5c]}')
  jwks+=("$jwk")

  # Clean up
  rm public_key.pem
done

# Write array of JWKs to file
echo "${jwks[@]}" > jwk/jwks.txt

# Convert array of JWKs to JSON object
jwks_obj=$(jq -s '.' jwk/jwks.txt)

# Write JSON object to file
echo "$jwks_obj" > jwk/jwks.json

# Clean up
rm jwk/jwks.txt

echo "JWK files saved in jwk/jwks.json"
