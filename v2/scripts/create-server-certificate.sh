#!/bin/bash

# Set the path to the OpenSSL configuration file
# OPENSSL_CONFIG="/etc/ssl/openssl.cnf"
OPENSSL_CONFIG="../config/openssl.cnf"

# Set the CA directory
CA_DIR="./demoCA"

# Set the server certificate directory
SERVER_DIR="./server"

# Set the certificate and private key file names
CERT_FILE="$SERVER_DIR/server.crt"
KEY_FILE="$SERVER_DIR/server.key"

# Set the PKCS12 file name
PKCS12_FILE="$SERVER_DIR/server.p12"

# Set the default values for the certificate information
COUNTRY_DEFAULT="CA"
STATE_DEFAULT="Ontario"
CITY_DEFAULT="Toronto"
ORG_DEFAULT="Acme Inc."
OU_DEFAULT="Technology"
COMMON_NAME_DEFAULT="Acme Certificate Authority"
EMAIL_DEFAULT="admin@acme.com"

# Prompt the user for the certificate information
read -rp "Country Name (2 letter code) [$COUNTRY_DEFAULT]: " COUNTRY
COUNTRY=${COUNTRY:-$COUNTRY_DEFAULT}
read -rp "State or Province Name (full name) [$STATE_DEFAULT]: " STATE
STATE=${STATE:-$STATE_DEFAULT}
read -rp "Locality Name (eg, city) [$CITY_DEFAULT]: " CITY
CITY=${CITY:-$CITY_DEFAULT}
read -rp "Organization Name (eg, company) [$ORG_DEFAULT]: " ORG
ORG=${ORG:-$ORG_DEFAULT}
read -rp "Organizational Unit Name (eg, IT department) [$OU_DEFAULT]: " OU
OU=${OU:-$OU_DEFAULT}
read -rp "Common Name (eg, your name or your server's hostname) [$COMMON_NAME_DEFAULT]: " COMMON_NAME
COMMON_NAME=${COMMON_NAME:-$COMMON_NAME_DEFAULT}
read -rp "Email Address [$EMAIL_DEFAULT]: " EMAIL
EMAIL=${EMAIL:-$EMAIL_DEFAULT}

# Create the server directory if it doesn't exist
if [ ! -d "$SERVER_DIR" ]; then
    mkdir "$SERVER_DIR"
fi

# Generate a private key for the server
openssl genrsa -out "$KEY_FILE" 4096
chmod 400 "$KEY_FILE"


# Generate a certificate signing request (CSR) for the server
openssl req -new \
  -key "$KEY_FILE" \
  -out "$CERT_FILE.csr" \
  -subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$ORG/OU=$OU/CN=$COMMON_NAME/emailAddress=$EMAIL"

# Sign the CSR with the CA certificate and key to generate a server certificate
openssl ca \
  -in "$CERT_FILE.csr" \
  -out "$CERT_FILE" \
  -keyfile "$CA_DIR/ca.key" \
  -cert "$CA_DIR/ca.crt" \
  -extensions server_cert \
  -config "$OPENSSL_CONFIG"

# Combine the server certificate and private key into a PKCS12 file
openssl pkcs12 \
  -export \
  -in "$CERT_FILE" \
  -inkey "$KEY_FILE" \
  -out "$PKCS12_FILE" \
  -name "$COMMON_NAME"

# Display the PKCS12 file information
openssl pkcs12 -in "$PKCS12_FILE" -info
