#!/bin/bash

CN="TLS AC Raiz bCadastros DRCA3 DES v1"
SUBJ="/C=BR/O=SERPRO/OU=bCadastros-CIN/CN=${CN}"
DAYS=3650
CABASE="./TLS/ROOT"
CERTIFICATE=${CABASE}/tlsca.pem
PRIVATE_KEY=${CABASE}/tlsca.key

mkdir -p "${CABASE}/"{certs,newcerts,private,requests}

openssl genrsa -out $PRIVATE_KEY 2048

touch "${CABASE}/index.txt"
touch "${CABASE}/index.txt.attr"
echo 00 > "${CABASE}/serial"
cat <<< "[ ca ]
default_ca             = CA_default
[ CA_default ]
dir                    = ${CABASE} # Where everything is kept
certs                  = ${CABASE}/certs # Where the issued certs are kept
crl_dir                = ${CABASE}/crl # Where the issued crl are kept
unique_subject         = no # Set to 'no' to allow creation of several certs with same subject.
database               = ${CABASE}/index.txt # database index file.
new_certs_dir          = ${CABASE}/newcerts # default place for new certs.
certificate	           = ${CERTIFICATE} # The CA certificate
serial		           = ${CABASE}/serial # The current serial number
crlnumber	           = ${CABASE}/crlnumber # the current crl number must be commented out to leave a V1 CRL
crl		               = ${CABASE}/crl.pem # The current CRL
private_key 	       = ${PRIVATE_KEY} # The private key
default_days           = 730
policy                 = policy_anything
email_in_dn            = no
[policy_anything]
countryName            = optional
stateOrProvinceName    = optional
localityName           = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional
serialNumber           = supplied
businessCategory       = optional
jurisdictionCountryName = optional
[req]
req_extensions = v3_ca
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_ca ]
basicConstraints = critical, CA:TRUE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always, issuer
keyUsage = critical, cRLSign, keyCertSign, digitalSignature, keyEncipherment
[ v3_TLS ]
basicConstraints = critical, CA:TRUE
subjectKeyIdentifier = hash
authorityKeyIdentifier= keyid, issuer
keyUsage = critical, cRLSign, keyCertSign, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth, clientAuth
" > "${CABASE}/ca.conf"

openssl req -new -x509 -days $DAYS -subj "$SUBJ" -key $PRIVATE_KEY -config  "${CABASE}/ca.conf" -extensions v3_TLS -out $CERTIFICATE
