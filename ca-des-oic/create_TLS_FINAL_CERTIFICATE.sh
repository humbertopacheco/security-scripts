#!/bin/bash
DNS=$1
CNPJ=$2
SUBJ="/C=BR/ST=DF/L=BRASILIA/O=SERPRO/OU=bCadastros CIN/OU=Desenvolvimento/OU=serprodrive/serialNumber=$CNPJ/CN=$DNS/businessCategory=Government Entity/jurisdictionCountryName=BR"
DAYS=3650
SIGNED_BY="./TLS/ROOT"
BASEDIR="./TLS/$DNS"
FILEBASENAME="$BASEDIR/$DNS"
CERTIFICATE=${FILEBASENAME}.crt
REQUEST=${FILEBASENAME}.request
PRIVATE_KEY=${FILEBASENAME}.key

rm -f "${PRIVATE_KEY}"
rm -f "${REQUEST}"
rm -f "${CERTIFICATE}"

mkdir -p $BASEDIR
openssl genrsa -out $PRIVATE_KEY 2048

cat <<< "
subjectAltName=DNS:${DNS}
basicConstraints = critical, CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier= keyid, issuer
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth, clientAuth
" > "./${FILEBASENAME}-server"

echo openssl req -new -subj "$SUBJ" -key ${PRIVATE_KEY} -out ${REQUEST}
openssl req -new -subj "$SUBJ" -key ${PRIVATE_KEY} -out ${REQUEST}

echo openssl ca -md sha256 -batch -notext -days $DAYS -config ${SIGNED_BY}/ca.conf -extfile ${FILEBASENAME}-server -in ${REQUEST} -out ${CERTIFICATE}
openssl ca -md sha256 -batch -notext -days $DAYS -config ${SIGNED_BY}/ca.conf -extfile ${FILEBASENAME}-server -in ${REQUEST} -out ${CERTIFICATE}
