# AC de desenvolvimento para testes com OICs

Arquivo da AC gerada: `TLS/ROOT/tlsca.pem`

Para criar um novo certificado final deve ser utilizado o comando `create_TLS_FINAL_CERTIFICATE.sh` com os parâmetros DNS e CNPJ.

Exemplo:
```
bash create_TLS_FINAL_CERTIFICATE.sh dfcdsrvv10978.srv.cd.serpro 33683111000441
```
